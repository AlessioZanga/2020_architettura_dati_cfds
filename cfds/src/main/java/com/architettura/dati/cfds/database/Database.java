package com.architettura.dati.cfds.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Database<T> {
	
	protected Connection connection;
	protected List<String> schema;
	protected List<ArrayList<T>> rows;
	
	public Database() { }
	
	public Database(String url, String username, String password) throws SQLException {
		connection = DriverManager.getConnection(url, username, password);
		schema = new ArrayList<String>();
		rows = new ArrayList<ArrayList<T>>();
		buildSchemaAndRows();
	}
	
	public Connection getConnection() {
		return connection;
	}

	public List<String> getSchema() {
		return schema;
	}

	public List<ArrayList<T>> getRows() {
		return rows;
	}
	
	public void setRows(List<ArrayList<T>> rows) {
		this.rows = rows;
	}

	@SuppressWarnings("unchecked")
	protected void buildSchemaAndRows() throws SQLException {
		Statement query = connection.createStatement();
		ResultSet result = query.executeQuery("SELECT * FROM data");
		ResultSetMetaData metadata = result.getMetaData();
		int columnsCount = metadata.getColumnCount();
		
		for (int i = 1; i <= columnsCount; i++) {
			schema.add(metadata.getColumnLabel(i));
		}
		
		while (result.next()) {
			ArrayList<T> row = new ArrayList<T>();
			for (int i = 1; i <= columnsCount; i++) {
				row.add((T) result.getObject(i));
			}
			rows.add(row);
		}
	}

	@Override
	public String toString() {
		String columns = "Columns [";
		for (String c: schema) {
			columns += c + ", ";
		}
		columns += "]";
		return "Database [schema=" + columns + "]";
	}

}
