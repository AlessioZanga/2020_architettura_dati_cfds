package com.architettura.dati.cfds.gcgrowth;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.architettura.dati.cfds.fptree.FPNode;
import com.architettura.dati.cfds.fptree.FPTree;
import com.architettura.dati.cfds.fptree.Node;
import com.architettura.dati.cfds.structures.LHS;

@SuppressWarnings("rawtypes")
public class GCGrowth<T> {
	
	private FPTree<Entry> tree;
	
	public GCGrowth(FPTree<Entry> tree) {
		this.tree = tree;
	}
	
	private int computeSetSupport(LHS<T> Xi) {
		int count = 0;
		Map<Entry, List<FPNode<Entry>>> nodelinks = tree.getTable().getNodelinks();
		for (FPNode<Entry> node: nodelinks.get(Xi.getLastEntry())) {
			LHS<T> X1 = new LHS<T>(node);
			if (Xi.isSubset(X1)) {
				count += node.getCount();
			}
		}
		return count;
	}
	
	private boolean checkSetMinimumSupport(Map<LHS<T>, Integer> supports, LHS<T> Xi) {
		return supports.get(Xi) >= tree.getMinimumSupport();
	}
	
	private boolean checkSubsetSupports(Map<LHS<T>, Integer> supports, LHS<T> Xi) {
		for (LHS<T> combination: Xi.getCombinations()) {
			if (supports.get(Xi) >= supports.get(combination)) {
				return false;
			}
		}
		return true;
	}
	
	private boolean checkSubsetGenerators(Map<LHS<T>, Boolean> generators, LHS<T> Xi) {
		for (LHS<T> combination: Xi.getCombinations()) {
			if (!generators.get(combination)) {
				return false;
			}
		}
		return true;
	}
	
	private List<LHS<T>> computeX2(FPNode<Entry> node) {
		List<LHS<T>> X2 = new ArrayList<LHS<T>>();
		for (Node<Entry> leaf: node.getLeaves()) {
			X2.add(new LHS<T>((FPNode<Entry>) leaf));
		}
		return X2;
	}
	
	private LHS<T> computeIntersectC(LHS<T> Xi) {
		List<LHS<T>> C = new ArrayList<LHS<T>>();
		Map<Entry, List<FPNode<Entry>>> nodelinks = tree.getTable().getNodelinks();
		for (FPNode<Entry> node: nodelinks.get(Xi.getLastEntry())) {
			LHS<T> X1 = new LHS<T>((FPNode<Entry>) node); 
			if (Xi.isSubset(X1)) {
				C.addAll(computeX2(node));
			}
		}
		LHS<T> intersectC = C.get(0);
		for (LHS<T> X2: C) {
			intersectC = intersectC.intersect(X2);
		}
		return intersectC;
	}
	
	public Map<LHS<T>, List<LHS<T>>> run() {
		Map<LHS<T>, Boolean> generators = new LinkedHashMap<LHS<T>, Boolean>();
		Map<LHS<T>, Integer> supports = new LinkedHashMap<LHS<T>, Integer>();
		Map<LHS<T>, List<LHS<T>>> result = new LinkedHashMap<LHS<T>, List<LHS<T>>>();
		
		LHS<T> empty = new LHS<T>(tree.getRoot());
		generators.put(empty, true);
		supports.put(empty, Integer.MAX_VALUE);
		
		for (int i = 1; i < tree.sizeXi(); i++) {
			LHS<T> Xi = new LHS<T>(tree.getXi(i));
			supports.put(Xi, computeSetSupport(Xi));
			if (checkSetMinimumSupport(supports, Xi) &&
				checkSubsetSupports(supports, Xi) &&
				checkSubsetGenerators(generators, Xi)) {
				generators.put(Xi, true);
				LHS<T> intersectC = computeIntersectC(Xi);
				if (!result.containsKey(intersectC)) {
					result.put(intersectC, new ArrayList<LHS<T>>());
				}
				result.get(intersectC).add(Xi);
			} else {
				generators.put(Xi, false);
			}
		}
		
		Map<LHS<T>, List<LHS<T>>> tmp = new LinkedHashMap<LHS<T>, List<LHS<T>>>();
		for (LHS<T> key: result.keySet()) {
			if (!result.get(key).isEmpty()) {
				List<LHS<T>> values = result.get(key);
				for (LHS<T> value: values) {
					value.removeEntry(tree.getRoot().getValue());
				}
				key.removeEntry(tree.getRoot().getValue());
				tmp.put(key, values);
			}
		}
		result = tmp;
		
		return result;
	}

}
