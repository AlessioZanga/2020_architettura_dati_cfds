package com.architettura.dati.cfds.cfdminer;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.architettura.dati.cfds.structures.CFD;
import com.architettura.dati.cfds.structures.LHS;
import com.architettura.dati.cfds.structures.RHS;

public class CFDMiner<T> {
	
	Map<LHS<T>, List<LHS<T>>> closedToFree;
	
	public CFDMiner(Map<LHS<T>, List<LHS<T>>> closedToFree) {
		this.closedToFree = closedToFree;
	}
	
	public List<CFD<T>> run() {
		List<CFD<T>> result = new ArrayList<CFD<T>>();
		
		for (LHS<T> closedSet: closedToFree.keySet()) {
			List<LHS<T>> freeSetList = closedToFree.get(closedSet);
			Map<LHS<T>, LHS<T>> candidateRHSMap = new LinkedHashMap<LHS<T>, LHS<T>>();
			
			for (LHS<T> freeSet: freeSetList) {
				LHS<T> candidateRHS = closedSet.subtract(freeSet);
				candidateRHSMap.put(freeSet, candidateRHS);
			}
			
			for (LHS<T> freeSet: freeSetList) {
				List<LHS<T>> subSetList = freeSet.getStrictPowerset();
				
				for (LHS<T> subSet: subSetList) {
					LHS<T> freeSetRHS = candidateRHSMap.get(freeSet);
					if (candidateRHSMap.containsKey(subSet)) {
						LHS<T> subSetRHS = candidateRHSMap.get(subSet);
						candidateRHSMap.put(freeSet, freeSetRHS.intersect(subSetRHS));
					}
				}
				
				LHS<T> candidateRHS = candidateRHSMap.get(freeSet);
				int size = candidateRHS.getX().size();
				if (size > 0) {
					for (int i = 0; i < size; i++) {
						RHS<T> rhs = new RHS<T>(
								candidateRHS.getX(i),
								candidateRHS.getTpx(i)
							);
						CFD<T> cdf = new CFD<T>(freeSet, rhs);
						result.add(cdf);
					}
				}
			}
		}
		
		return result;
	}

}
