package com.architettura.dati.cfds.fptree;

public class FPNode<T> extends Node<T>{
	
	private int count;
	private int level;

	public FPNode() { }
	
	public FPNode(T value) {
		super(value);
		count = 0;
		level = 0;
	}

	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public void incrementCount() {
		count++;
	}

}
