package com.architettura.dati.cfds.fptree;

@SuppressWarnings("hiding")
public class Tree<Node> {
	
	protected Node root;
	
	public Tree(Node root) {
		this.root = root;
	}

	public Node getRoot() {
		return root;
	}

}
