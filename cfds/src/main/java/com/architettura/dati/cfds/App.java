package com.architettura.dati.cfds;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.AbstractMap.SimpleEntry;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.architettura.dati.cfds.cfdminer.CFDMiner;
import com.architettura.dati.cfds.database.Database;
import com.architettura.dati.cfds.database.EntryDatabase;
import com.architettura.dati.cfds.fptree.FPTree;
import com.architettura.dati.cfds.gcgrowth.GCGrowth;
import com.architettura.dati.cfds.structures.CFD;
import com.architettura.dati.cfds.structures.LHS;

public class App 
{
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main( String[] args ) throws SQLException
    {
    	CommandLine cmd;
    	
    	Option help = Option.builder("h")
    		.required(false)
    		.desc("The help")
    		.longOpt("help")
    		.build();
    	Option db = Option.builder("d")
    		.required(true)
    		.desc("The database name")
    		.longOpt("database")
    		.hasArg()
    		.build();
    	Option user = Option.builder("u")
    		.required(true)
    		.desc("The database username")
    		.longOpt("user")
    		.hasArg()
    		.build();
    	Option pass = Option.builder("p")
    		.required(true)
    		.desc("The database username password")
    		.longOpt("pass")
    		.hasArg()
    		.build();
    	Option support = Option.builder("s")
    		.required(true)
    		.desc("The minimum support")
    		.longOpt("support")
    		.hasArg()
    		.build();
    	
    	Options options = new Options();
        CommandLineParser parser = new DefaultParser();
        
        options.addOption(help);
        options.addOption(db);
        options.addOption(user);
        options.addOption(pass);
        options.addOption(support);
        
        try {
			cmd = parser.parse(options, args);
			
			Database<Entry> database = new EntryDatabase<String>(
				"jdbc:mysql://localhost/" + cmd.getOptionValue("database"),
				cmd.getOptionValue("user"),
				cmd.getOptionValue("pass")
			);
	        
	        Entry root = new SimpleEntry("root", "null");
	        
	        FPTree<Entry> tree;
	        try {
	        	tree = new FPTree<Entry>(
            		root,
            		database,
            		Integer.parseInt(cmd.getOptionValue("support"))
            	);
	        } catch (NumberFormatException e) {
	        	tree = new FPTree<Entry>(
            		root,
            		database,
            		Double.parseDouble(cmd.getOptionValue("support"))
            	);
	        }
	    	
	    	GCGrowth<String> gcgrowth = new GCGrowth<String>(tree);
	    	Map<LHS<String>, List<LHS<String>>> closedToFree = gcgrowth.run();
	    	
	    	CFDMiner<String> cfdminer = new CFDMiner<String>(closedToFree);
	    	List<CFD<String>> cfds = cfdminer.run();
	    	
	    	for (CFD<String> cfd: cfds) {
	    		System.out.println(cfd);
	    	}
		} catch (ParseException e) {
			e.printStackTrace();
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("cfds", options );
		}
    }
}
