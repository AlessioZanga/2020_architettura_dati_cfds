package com.architettura.dati.cfds.structures;

import java.util.Map.Entry;
import java.util.AbstractMap.SimpleEntry;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class RHS<T> {
	
	private Entry entry;
	
	public RHS(String A, T tpa) {
		entry = new SimpleEntry<String, T>(A, tpa);
	}

	public String getA() {
		return (String) entry.getKey();
	}

	public T getTpa() {
		return (T) entry.getValue();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entry == null) ? 0 : entry.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RHS other = (RHS) obj;
		if (entry == null) {
			if (other.entry != null)
				return false;
		} else if (!entry.equals(other.entry))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RHS [entry=" + entry + "]";
	}

}
