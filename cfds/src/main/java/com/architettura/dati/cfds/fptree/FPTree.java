package com.architettura.dati.cfds.fptree;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.architettura.dati.cfds.database.Database;
import com.architettura.dati.cfds.utils.Utils;

public class FPTree<T> extends Tree<FPNode<T>>{

	protected Database<T> database;
	protected int minimumSupport;

	protected FPTable<T> table;
	protected List<T> Xi;

	public FPTree(T root, Database<T> database, int absoluteMinimumSupport) {
		super(new FPNode<T>(root));
		this.database = database;
		this.minimumSupport = absoluteMinimumSupport;
		this.table = new FPTable<T>(database, minimumSupport);
		initTree();
	}
	
	public FPTree(T root, Database<T> database, double relativeMinimumSupport) {
		super(new FPNode<T>(root));
		this.database = database;
		this.minimumSupport = (int) (relativeMinimumSupport * database.getRows().size());
		this.table = new FPTable<T>(database, minimumSupport);
		initTree();
	}

	public Database<T> getDatabase() {
		return database;
	}

	public int getMinimumSupport() {
		return minimumSupport;
	}

	public FPTable<T> getTable() {
		return table;
	}

	private void initTree() {
		initXi();
		filterRows();
		buildTree();
	}
	
	private void initXi() {
		this.Xi = new ArrayList<T>(table.getCounter().keySet());
	}
	
	public List<T> getXi(long i) {
		long pointer = 1;
		List<T> subset = new ArrayList<T>();
		subset.add(root.getValue());
        for(int j = 0; j < Xi.size(); j++) {
            if((i & (pointer << j)) > 0) {
            	subset.add(Xi.get(j));
            }
        }
        return subset;
	}
	
	public int sizeXi() {
		return (int) Math.pow(2, Xi.size());
	}

	private void filterRows() {
		List<ArrayList<T>> filteredRows = new ArrayList<ArrayList<T>>();
		for (List<T> row: database.getRows()) {
			Map<T, Integer> filteredEntry = new LinkedHashMap<T, Integer>();
			for (T entry: row) {
				if (table.getCounter().containsKey(entry)) {
					filteredEntry.put(entry, table.getCounter().get(entry));
				}
			}
			Utils<T> utils = new Utils<T>();
			filteredEntry = utils.sortMapByIntegerKey(filteredEntry, true);
			ArrayList<T> filteredRow = new ArrayList<T>();
			filteredRow.addAll(filteredEntry.keySet());
			filteredRows.add(filteredRow);
		}
		database.setRows(filteredRows);
	}

	protected void buildTree() {
		for (ArrayList<T> row: database.getRows()) {
			int level = 0;
			FPNode<T> parent = root;
			for (T entry: row) {
				level++;
				FPNode<T> current = new FPNode<T>(entry);
				int index = parent.getChildren().indexOf(current);
				if (index < 0) {
					current.setParent(parent);
					current.setLevel(level);
					table.getNodelinks().get(entry).add(current);
					index = parent.getChildren().indexOf(current);
				}
				current = (FPNode<T>) parent.getChildByIndex(index);
				current.incrementCount();
				parent = current;
			}
		}
	}
	
	public void printTree() {
		printTree(root);
		System.out.println();
	}
	
	private void printTree(FPNode<T> node) {
		System.out.print(node.getValue() + " = " + node.getCount() + ", ");
		if (!node.isLeaf()) {
			for(Node<T> child: node.getChildren()) {
				System.out.println();
				for (int i = 0; i < ((FPNode<T>) child).getLevel(); i++) {
					System.out.print("   ");
				}
				System.out.print("[");
				printTree((FPNode<T>) child);
				System.out.print("]");
			}
		}
	}

	@Override
	public String toString() {
		return "FPTree [minimumSupport=" + minimumSupport + ", table=" + table + "]";
	}

}
