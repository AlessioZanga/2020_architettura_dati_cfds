package com.architettura.dati.cfds.fptree;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.architettura.dati.cfds.database.Database;
import com.architettura.dati.cfds.utils.Utils;

public class FPTable<T> {

	private Map<T, Integer> counter;
	private Map<T, List<FPNode<T>>> nodelinks;

	public FPTable(Database<T> database, int minimumSupport) {
		this.counter = new LinkedHashMap<T, Integer>();
		this.nodelinks = new LinkedHashMap<T, List<FPNode<T>>>();
		initCounter(database, minimumSupport);
		initNodeLinks();
	}
	
	public Map<T, Integer> getCounter() {
		return counter;
	}

	public Map<T, List<FPNode<T>>> getNodelinks() {
		return nodelinks;
	}

	private void initCounter(Database<T> database, int minimumSupport) {
		buildCounter(database);
		tresholdCounter(minimumSupport);
		sortCounter();
	}

	protected void buildCounter(Database<T> database) {
		for (ArrayList<T> row : database.getRows()) {
			for (T entry : row) {
				int count = counter.getOrDefault(entry, 0);
				counter.put(entry, count + 1);
			}
		}
	}

	private void tresholdCounter(int minimumSupport) {
		Map<T, Integer> newCounter = new LinkedHashMap<T, Integer>();
		for (T key : counter.keySet()) {
			int count = counter.get(key);
			if (count >= minimumSupport) {
				newCounter.put(key, count);
			}
		}
		counter = newCounter;
	}

	private void sortCounter() {
		Utils<T> utils = new Utils<T>();
		counter = utils.sortMapByIntegerKey(counter, true);
	}
	
	private void initNodeLinks() {
		buildNodeLinks();
	}

	private void buildNodeLinks() {
		for (T key : counter.keySet()) {
			nodelinks.put(key, new ArrayList<FPNode<T>>());
		}
	}
	
	public void printNodeLinks() {
		for(T key: nodelinks.keySet()) {
			String links = "Key = " + key + ", Links [";
			for (FPNode<T> node: nodelinks.get(key)) {
				links += node.getValue() + " = " + node.getCount() + ", ";
			}
			links += "], ";
			System.out.println(links);
		}
	}

	@Override
	public String toString() {
		String counter = "Counter [";
		for (T key : this.counter.keySet()) {
			counter += key + " = " + this.counter.get(key) + ", ";
		}
		counter += "]";

		String nodelinks = "NodeLinks [Use the printNodeLinks method]";

		return "FPTable [counter=" + counter + ", nodelinks=" + nodelinks + "]";
	}
}
