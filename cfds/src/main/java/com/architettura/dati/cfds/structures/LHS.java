package com.architettura.dati.cfds.structures;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.AbstractMap.SimpleEntry;

import com.architettura.dati.cfds.fptree.FPNode;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class LHS<T> {
	
	private Set<Entry> entries = new LinkedHashSet<Entry>();
	
	public LHS() { }
	
	public LHS(List<String> X, List<T> tpx) {
		for (int i = 0; i < X.size(); i++) {
			Entry entry = new SimpleEntry<String, T>(X.get(i), tpx.get(i));
			entries.add(entry);
		}
	}
	
	public LHS(List<Entry> entries) {
		this.entries.addAll(entries);
	}
	
	public LHS(FPNode<Entry> node) {
		entries.addAll(node.getPatternValues());
	}

	public List<String> getX() {
		List<String> X = new ArrayList<String>();
		for (Entry entry: entries) {
			X.add((String) entry.getKey());
		}
		return X;
	}
	
	public String getX(int index) {
		return (String) getEntry(index).getKey();
	}
	
	public List<T> getTpx() {
		List<T> tpx = new ArrayList<T>();
		for (Entry entry: entries) {
			tpx.add((T) entry.getValue());
		}
		return tpx;
	}
	
	public T getTpx(int index) {
		return (T) getEntry(index).getValue();
	}
	
	public Entry getEntry(int index) {
		for (Entry entry: entries) {
			if (index == 0) {
				return entry;
			}
			index--;
		}
		return null;
	}
	
	public Entry getLastEntry() {
		return getEntry(entries.size() - 1);
	}
	
	public boolean removeEntry(Entry entry) {
		return entries.remove(entry);
	}
	
	public boolean isSubset(LHS<T> other) {
		return other.entries.containsAll(entries);
	}
	
	public LHS<T> getSubset(Set<String> X) {
		List<Entry> filtered = new ArrayList<Entry>(entries);
		filtered.removeIf(entry -> !X.contains((String) entry.getKey()));
		return new LHS<T>(filtered);
	}
	
	public List<LHS<T>> getCombinations() {
		List<LHS<T>> result = new ArrayList<LHS<T>> ();
		for (int i = entries.size() - 1; i > 0; i--) {
			List<Entry> combinations = new ArrayList<Entry>();
			for (int j = 0; j < entries.size(); j++) {
				if (i != j) {
					combinations.add(getEntry(j));
				}
			}
			result.add(new LHS<T>(combinations));
		}
		return result;
	}
	
	public List<LHS<T>> getPowerset() {
		List<LHS<T>> powerset = new ArrayList<LHS<T>>();
		for(long i = 0; i < (int) Math.pow(2, entries.size()); i++) {
			long pointer = 1;
			Set<String> index = new LinkedHashSet<String>();
            for(int j = 0; j < entries.size(); j++) {
                if((i & (pointer << j)) > 0) {
                	index.add((String) getEntry(j).getKey());
                }
            }
            powerset.add(getSubset(index));
        }
		return powerset;
	}
	
	public List<LHS<T>> getStrictPowerset() {
		return getPowerset().subList(1, (int) (Math.pow(2, entries.size()) - 1));
	}
	
	public LHS<T> subtract(LHS<T> other) {
		List<Entry> filtered = new ArrayList<Entry>(entries);
		filtered.removeIf(entry -> other.entries.contains(entry));
		return new LHS<T>(filtered);
	}
	
	public LHS<T> intersect(LHS<T> other) {
		List<Entry> filtered = new ArrayList<Entry>(entries);
		filtered.removeIf(entry -> !other.entries.contains(entry));
		return new LHS<T>(filtered);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entries == null) ? 0 : entries.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LHS other = (LHS) obj;
		if (entries == null) {
			if (other.entries != null)
				return false;
		} else if (!entries.equals(other.entries))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String entries = "Entries [";
		for (Entry entry: this.entries) {
			entries += entry + ", ";
		}
		entries += "]";
		return "LHS [entries=" + entries + "]";
	}

}
