package com.architettura.dati.cfds.fptree;

import java.util.ArrayList;
import java.util.List;

public class Node<T> {

	private T value;
	private Node<T> parent;
	private List<Node<T>> children = new ArrayList<Node<T>>();
	
	public Node() { }
	
	public Node(T value) {
		this.value = value;
	}

	public T getValue() {
		return value;
	}
	
	public void setValue(T value) {
		this.value = value;
	}
	
	public Node<T> getParent() {
		return parent;
	}
	
	public void setParent(Node<T> parent) {
		this.parent = parent;
		if (!parent.children.contains(this)) {
			parent.addChild(this);
		}
	}
	
	public void addChild(Node<T> node) {
		children.add(node);
	}
	
	public Node<T> getChildByIndex(int index) {
		return children.get(index);
	}

	public List<Node<T>> getChildren() {
		return children;
	}
	
	public List<Node<T>> getPatternNodes() {
		Node<T> node = this;
		List<Node<T>> pattern = new ArrayList<Node<T>>();
		while (node != null) {
			pattern.add(0, node);
			node = node.getParent();
		}
		return pattern;
	}
	
	public List<T> getPatternValues() {
		Node<T> node = this;
		List<T> pattern = new ArrayList<T>();
		while (node != null) {
			pattern.add(0, node.getValue());
			node = node.getParent();
		}
		return pattern;
	}
	
	public boolean isLeaf() {
		return children.isEmpty();
	}
	
	public List<Node<T>> getLeaves() {
		return getLeaves(this);
	}
	
	private List<Node<T>> getLeaves(Node<T> node) {
		List<Node<T>> leaves = new ArrayList<Node<T>>();
		if (node.isLeaf()) {
			leaves.add(node);
		} else {
			for (Node<T> child: children) {
				leaves.addAll(child.getLeaves());
			}
		}
		return leaves;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node<T> other = (Node<T>) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}
