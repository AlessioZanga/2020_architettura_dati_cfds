package com.architettura.dati.cfds.utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Utils<T> {

	public Map<T, Integer> sortMapByIntegerKey(Map<T, Integer> filteredEntry, boolean reversed) {
		List<Entry<T, Integer>> sortedList = new LinkedList<Entry<T, Integer>>();
		sortedList.addAll(filteredEntry.entrySet());
		
		Comparator<Entry<T, Integer>> comparator = (new Comparator<Entry<T, Integer>>() {
			public int compare(Entry<T, Integer> a, Entry<T, Integer> b) {
				return a.getValue().compareTo(b.getValue());
			}
		});

		if (reversed) {
			comparator = (new Comparator<Entry<T, Integer>>() {
				public int compare(Entry<T, Integer> a, Entry<T, Integer> b) {
					return -(a.getValue().compareTo(b.getValue()));
				}
			});
		}
		
		Collections.sort(
			sortedList,
			comparator
		);
		
		Map<T, Integer> sortedMap = new LinkedHashMap<T, Integer>();
		for (Entry<T, Integer> entry: sortedList) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}

}
