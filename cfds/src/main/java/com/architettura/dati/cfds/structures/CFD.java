package com.architettura.dati.cfds.structures;

public class CFD<T> {

	private LHS<T> lhs;
	private RHS<T> rhs;
	
	public CFD(LHS<T> lhs, RHS<T> rhs) {
		this.lhs = lhs;
		this.rhs = rhs;
	}

	public LHS<T> getLhs() {
		return lhs;
	}

	public void setLhs(LHS<T> lhs) {
		this.lhs = lhs;
	}

	public RHS<T> getRhs() {
		return rhs;
	}

	public void setRhs(RHS<T> rhs) {
		this.rhs = rhs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lhs == null) ? 0 : lhs.hashCode());
		result = prime * result + ((rhs == null) ? 0 : rhs.hashCode());
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CFD<T> other = (CFD<T>) obj;
		if (lhs == null) {
			if (other.lhs != null)
				return false;
		} else if (!lhs.equals(other.lhs))
			return false;
		if (rhs == null) {
			if (other.rhs != null)
				return false;
		} else if (!rhs.equals(other.rhs))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CFD [lhs=" + lhs + ", rhs=" + rhs + "]";
	}

}
