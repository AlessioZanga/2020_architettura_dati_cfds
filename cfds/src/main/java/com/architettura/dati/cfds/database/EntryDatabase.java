package com.architettura.dati.cfds.database;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Map.Entry;

@SuppressWarnings("rawtypes")
public class EntryDatabase<T> extends Database<Entry> {
	
	public EntryDatabase() { }

	public EntryDatabase(String url, String username, String password) throws SQLException {
		super(url, username, password);
	}
	
	@SuppressWarnings({ "unchecked" })
	@Override
	protected void buildSchemaAndRows() throws SQLException {
		Statement query = connection.createStatement();
		ResultSet result = query.executeQuery("SELECT * FROM data");
		ResultSetMetaData metadata = result.getMetaData();
		int columnsCount = metadata.getColumnCount();
		
		for (int i = 1; i <= columnsCount; i++) {
			schema.add(metadata.getColumnLabel(i));
		}
		
		while (result.next()) {
			ArrayList<Entry> row = new ArrayList<Entry>();
			for (int i = 1; i <= columnsCount; i++) {
				Entry entry = new SimpleEntry(
						schema.get(i-1),
						(T) result.getObject(i)
					);
				row.add(entry);
			}
			rows.add(row);
		}
	}
	
	
}
