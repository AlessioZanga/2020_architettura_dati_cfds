package com.architettura.dati.cfds;

import static org.junit.Assert.*;

import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import com.architettura.dati.cfds.database.Database;
import com.architettura.dati.cfds.database.EntryDatabase;

public class TestDatabase {
	
	@Test
	public void TestDatabaseChess() throws SQLException {
		Database<String> database = new Database<String>(
			"jdbc:mysql://localhost/reference",
			"root",
			"secret"
		);
		
		assertNotNull(database.getConnection());
        
        List<String> schema = new ArrayList<String>();
        schema.add("column_1");
        schema.add("column_2");
		schema.add("column_3");
		schema.add("column_4");
        assertEquals(database.getSchema(), schema);
        
        int rows = 5;
        assertEquals(database.getRows().size(), rows);
	}
	
	@Test
	@SuppressWarnings("rawtypes")
	public void TestEntryDatabaseChess() throws SQLException {
		Database<Entry> database = new EntryDatabase<String>(
			"jdbc:mysql://localhost/reference",
			"root",
			"secret"
		);
		
		assertNotNull(database.getConnection());
        
        List<String> schema = new ArrayList<String>();
        schema.add("column_1");
        schema.add("column_2");
		schema.add("column_3");
		schema.add("column_4");
        assertEquals(database.getSchema(), schema);
        
        int rows = 5;
        assertEquals(database.getRows().size(), rows);
	}

}
