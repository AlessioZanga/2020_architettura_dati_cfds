package com.architettura.dati.cfds;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.architettura.dati.cfds.fptree.FPNode;

public class TestFPNode {

	@Test
	public void TestFPNodeBase() {
		FPNode<String> node1 = new FPNode<String>("ABC");
		FPNode<String> node2 = new FPNode<String>("ABC");
		assertEquals(node1, node2);
		assertEquals(node1.getValue(), node2.getValue());
		
		node1.setCount(1);
		node2.setCount(2);
		assertEquals(node1, node2);
		assertTrue(node1.getCount() != node2.getCount());
		
		node1.setLevel(1);
		node2.setLevel(2);
		assertEquals(node1, node2);
		assertTrue(node1.getLevel() != node2.getLevel());
		
		node1.setValue("DEF");
		assertTrue(node1 != node2);
		assertTrue(node1.getValue() != node2.getValue());
		
		FPNode<String> parent = new FPNode<String>();
		node1.setParent(parent);
		node2.setParent(parent);
		assertTrue(parent.getChildren().contains(node1));
		assertTrue(parent.getChildren().contains(node2));
		assertEquals(node1.getParent(), node2.getParent());
		assertFalse(parent.isLeaf());
		assertTrue(node1.isLeaf());
		assertTrue(node2.isLeaf());
		
		List<FPNode<String>> node1patternnodes = new ArrayList<FPNode<String>>();
		node1patternnodes.add(parent);
		node1patternnodes.add(node1);
		assertEquals(node1.getPatternNodes(), node1patternnodes);
		
		List<String> node1patternvalues = new ArrayList<String>();
		node1patternvalues.add(null);
		node1patternvalues.add("DEF");
		assertEquals(node1.getPatternValues(), node1patternvalues);
		
		List<FPNode<String>> node2patternnodes = new ArrayList<FPNode<String>>();
		node2patternnodes.add(parent);
		node2patternnodes.add(node2);
		assertEquals(node2.getPatternNodes(), node2patternnodes);
		
		List<String> node2patternvalues = new ArrayList<String>();
		node2patternvalues.add(null);
		node2patternvalues.add("ABC");
		assertEquals(node2.getPatternValues(), node2patternvalues);
		
		FPNode<String> level0 = new FPNode<String>("Level0");
		FPNode<String> level1 = new FPNode<String>("Level1");
		level1.setParent(level0);
		FPNode<String> level2 = new FPNode<String>("Level2");
		level2.setParent(level1);
		FPNode<String> level3A = new FPNode<String>("Level3A");
		level3A.setParent(level2);
		FPNode<String> level3B = new FPNode<String>("Level3B");
		level3B.setParent(level2);
		FPNode<String> level4B = new FPNode<String>("Level4B");
		level4B.setParent(level3B);
		List<FPNode<String>> leaves = new ArrayList<FPNode<String>>();
		leaves.add(level3A);
		leaves.add(level4B);
		assertEquals(level0.getLeaves(), leaves);
	}

}
