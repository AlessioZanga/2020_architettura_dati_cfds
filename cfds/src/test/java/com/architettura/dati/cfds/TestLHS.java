package com.architettura.dati.cfds;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.AbstractMap.SimpleEntry;

import org.junit.Test;

import com.architettura.dati.cfds.structures.LHS;

public class TestLHS {

	@Test
	public void TestLHSBase() {
		LHS<String> lhs0 = new LHS<String>();
		assertEquals(lhs0, new LHS<String>());
		
		List<String> X = new ArrayList<String>();
		X.add("A");
		X.add("B");
		X.add("C");
		
		List<String> tpx = new ArrayList<String>();
		tpx.add("a");
		tpx.add("b");
		tpx.add("c");
		
		LHS<String> lhs1 = new LHS<String>(X, tpx);
		LHS<String> lhs2 = new LHS<String>(X, tpx);
		assertEquals(lhs1.getX(), X);
		assertEquals(lhs1.getX(0), X.get(0));
		assertEquals(lhs1.getTpx(), tpx);
		assertEquals(lhs1.getTpx(0), tpx.get(0));
		assertEquals(lhs1, lhs2);
		
		List<String> Y = new ArrayList<String>();
		Y.add("C");
		Y.add("B");
		Y.add("A");
		
		List<String> tpy = new ArrayList<String>();
		tpy.add("c");
		tpy.add("b");
		tpy.add("a");
		
		LHS<String> lhs3 = new LHS<String>(Y, tpy);
		assertEquals(lhs1, lhs3);
		
		Y.add("D");
		tpy.add("d");
		LHS<String> lhs4 = new LHS<String>(Y, tpy);
		assertTrue(lhs1 != lhs4);
		
		assertEquals(lhs4.getLastEntry(), new SimpleEntry<String, String>("D", "d"));
	}
	
	@Test
	@SuppressWarnings("rawtypes")
	public void TestLHSAdvanced() {
		List<String> X = new ArrayList<String>();
		X.add("A");
		X.add("B");
		X.add("C");
		
		List<String> tpx = new ArrayList<String>();
		tpx.add("a");
		tpx.add("b");
		tpx.add("c");
		
		LHS<String> lhs1 = new LHS<String>(X, tpx);
		
		Set<String> select = new LinkedHashSet<String>();
		select.add("A");
		select.add("B");
		List<Entry> subset = new ArrayList<Entry>();
		subset.add(new SimpleEntry<String, String>("A", "a"));
		subset.add(new SimpleEntry<String, String>("B", "b"));
		assertEquals(lhs1.getSubset(select), new LHS<String>(subset));
		
		List<LHS<String>> combinations = new ArrayList<LHS<String>>();
		combinations.add(new LHS<String>(subset.subList(0, 1)));
		assertEquals(lhs1.getSubset(select).getCombinations(), combinations);
		assertTrue(lhs1.getSubset(select).getCombinations().get(0).isSubset(combinations.get(0)));
		
		List<LHS<String>> powerset = new ArrayList<LHS<String>>();
		powerset.add(new LHS<String>());
		powerset.add(new LHS<String>(subset.subList(0, 1)));
		powerset.add(new LHS<String>(subset.subList(1, 2)));
		powerset.add(new LHS<String>(subset));
		assertEquals(lhs1.getSubset(select).getPowerset(), powerset);
		assertEquals(lhs1.getSubset(select).getStrictPowerset(), powerset.subList(1, powerset.size() - 1));
	}

}
