package com.architettura.dati.cfds;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.AbstractMap.SimpleEntry;

import org.junit.Test;

import com.architettura.dati.cfds.database.Database;
import com.architettura.dati.cfds.database.EntryDatabase;
import com.architettura.dati.cfds.fptree.FPNode;
import com.architettura.dati.cfds.fptree.FPTable;
import com.architettura.dati.cfds.fptree.FPTree;

public class TestFPTree {

	@Test
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void TestFPTreeChess() throws SQLException {
		Entry root = new SimpleEntry("root", "null");
		
		Database<Entry> database = new EntryDatabase<String>(
			"jdbc:mysql://localhost/reference",
			"root",
			"secret"
		);
        
		double minimumSupport = 0.40;
        FPTree<Entry> tree = new FPTree<Entry>(root, database, minimumSupport);
        
        assertEquals(tree.getDatabase(), database);
        
        FPTable<Entry> table = tree.getTable();
        Map<Entry, Integer> counter = new LinkedHashMap<Entry, Integer>();
        for (Entry key: table.getCounter().keySet()) {
        	int count = 0;
        	for (FPNode<Entry> node: table.getNodelinks().get(key)) {
        		count += node.getCount();
        	}
        	counter.put(key, count);
        }
        assertEquals(table.getCounter(), counter);
	}

}