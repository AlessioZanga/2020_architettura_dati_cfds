\documentclass{article}
\usepackage[a4paper,left=3cm,right=2cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amsmath,amssymb}
\usepackage{cite}
\usepackage{float}

\usepackage{color}
\definecolor{lightgray}{rgb}{0.96,0.96,0.96}

\usepackage{listings}
\lstset{
    backgroundcolor=\color{lightgray},
    frame=single,
    frameround=fttt,
    keepspaces=true,
    tabsize=2,
    breaklines=true,
    stepnumber=1,
    numbers=left,
    numbersep=5pt,
    morekeywords={for, each, all, do, end, if, then, else, true, false, and, or}
    }

\usepackage{hyperref}
\hypersetup{
	colorlinks,
	citecolor=black,
	filecolor=black,
	linkcolor=black,
	urlcolor=black
}

\title{Relazione del Progetto di Architetture Dati \\ - \\ Discovering Conditional Functional Dependecies (CFDs)}
\author{Cavenaghi Emanuele (816385) \\ Turano Andrea (816462) \\ Zanga Alessio (815997)}
\date{February 2020}

\begin{document}
\maketitle

\section{Introduzione}
Per identificare regole per correggere errori di consistenza nei database è necessario scoprire quali sono le relazioni presenti tra i dati. Per rendere automatico questo processo possono essere utilizzati i \textit{Conditional Functional Dependencies} (CFDs), che sono un'estensione dei \textit{Functional Dependencies} (FDs) ottenuti applicando pattern di attributi semanticamente correlati. 

Nello specifico, un FD è nella forma:
\begin{equation*}
    X \to A
\end{equation*}
\noindent
dove \textit{X} è un sottoinsieme degli attributi dello schema e \textit{A} è un singolo attributo dello schema.

I CFD estendono gli FD aggiungendo un ulteriore vincolo chiedendo che i valori degli attributi siano semanticamente correlati. Questi sono nella forma:
\begin{equation*}
    (X \to A, t_p)
\end{equation*}
\noindent
dove $t_p$ è una \textit{pattern tuple} con valori fissati per gli attributi di \textit{X} e di \textit{A}. I valori della \textit{pattern tuple} possono essere sia costanti, ossia valori specifici, sia variabili, indicati con "\_" che possono rappresentare qualsiasi valore del dominio dell'attributo corrispondente.

L'obiettivo prefissato del progetto è quindi quello di trovare i CFDs che possono essere utilizzati in una successiva fase di data quality per individuare inconsistenze da correggere nel dataset. 

\section{Definizione di CFDs}
Definiamo \textit{attr(R)} come il set di attributi di uno schema relazionale \textit{R} e \textit{dom(A)} come il dominio di un generico attributo $A \in attr(R)$. Possiamo quindi definire formalmente la sintassi e la semantica dei CFDs.

\subsection{Sintassi dei CFDs}
Un \textit{Conditional Functional Dependency} (CFD) $\phi$ su \textit{R} è una coppia 
\begin{equation*}
    (X \to A, t_p)
\end{equation*}
\noindent 
dove:
\begin{itemize}
    \item \textit{X} è un set di attributi in \textit{attr(R)} e \textit{A} è un singolo attributo in \textit{attr(R)}
    \item $X \to A$ è un FD standard riferito agli FD in $\phi$
    \item $t_p$ è una \textit{pattern tuple} con attributi in \textit{X} e in \textit{A} dove per ogni $B \in \{X \cup \{A\}\}$, $t_p[B]$ è una costante in \textit{dom(B)} o una variabile "\_" che può assumere qualsiasi valore in \textit{dom(B)}
\end{itemize}{}
\noindent
Denotiamo inoltre \textit{X} con LHS($\phi$) e \textit{A} con RHS($\phi$). In una \textit{pattern tuple} separeremo gli attributi di \textit{X} e di \textit{A} con $\parallel$. 

\subsection{Semantica dei CFDs}
Per dare una semantica ai CFDs dobbiamo definire un ordine $\leq$ sulle costanti e sulle variabili "\_", triviale da estendere alle tuple:
\begin{center}
    $\eta_1 \leq \eta_2$ se $\eta_1 = \eta_2$ oppure $\eta_1$  è una costante e $\eta_2$ è "\_"    
\end{center}
\noindent
Possiamo quindi dire che una tupla $t_1$ corrisponde alla tupla $t_2$ se $t_1 \leq t_2$. Scriveremo $t_1 \ll t_2$ se $t_1\leq t_2$ e $t_2 \nleq t_1$, cioè $t_2$ è più generale di $t_1$.

Un'istanza \textit{r} di \textit{R} soddisfa un CFD $\phi$, denotato con $r \models \phi$, se e solo se per ogni coppia di tuple $t_1, t_2 \in r$ se $t_1[X] = t_2[X] \leq t_p[X]$ allora $t_1[A] = t_2[A] \leq t_p[A]$. Possiamo quindi dire che un'istanza \textit{r} di \textit{R} soddisfa una set $\Sigma$ di CFDs su \textit{R} se:
\begin{equation*}
    \forall \phi \in \Sigma \qquad r \models \phi
\end{equation*}{}
\noindent
e lo denotiamo con $r \models \Sigma$.

Un CFD è detto \textbf{costante} se la sua \textit{pattern tuple} $t_p$ è composta solamente da costanti ed è detto \textbf{variabile} se $t_p[A] = "\_"$.  

\section{Complicazioni nella ricerca dei CFDs}
Data un'istanza \textit{r} di uno schema \textit{R}, un algoritmo per trovare i CFDs su \textit{r} dovrà restituire solamente un set minimale invece dell'intero set dei CFDs, contenente soluzioni triviali e ridondanti.

Inoltre nelle applicazioni reali è difficile, se non impossibile, trovare dataset che non contengano errori ed è quindi necessario escludere CFDs che individuano tuple inconsistenti. Per fare ciò considereremo solamente i \textit{frequent} CFDs aventi una \textit{pattern tuple} con supporto su \textit{r} maggiore di una certa soglia.

\subsection{Frequent CFDs}
Definiamo anzitutto il supporto di un CFD $\phi = (X \to A, t_p)$ in \textit{r}, denotato con $sup(\phi, r)$, come il set di tuple \textit{t} in \textit{r}  tali che $t[X] \leq t_p[X]$ e $t[A] \leq t_p[A]$.

Per un numero naturale $k\geq 1$ un CFDs $\phi$ è detto \textit{k-frequent} in \textit{r} se $|sup(\phi, r)| \geq k$.

\subsection{Minimal CFDs}
Un CFD costante $\phi$ su \textit{r} è detto \textbf{minimale} se è \textbf{non triviale} e \textbf{\textit{left-reduced}}. Diremo che un CFD $\phi = (X \to A, t_p)$ su R è \textbf{triviale} se e solo se $A \in X$.

Un CFD costante $(X \to A, (t_p \parallel a))$ è invece \textbf{\textit{left-reduced}} su \textit{r} se:
\begin{equation*}
    \forall Y \nsubseteq X \qquad r \nvDash (Y \to A, (t_p[Y] \parallel a)
\end{equation*}{}
\noindent
Questo ci assicura che:
\begin{itemize}
    \item Nessuno dei suoi attributi nell'LHS può essere rimosso
    \item Nessuna delle costanti nel pattern LHS può essere generalizzata a "\_", cioè il pattern $t_p[X]$ è quello più generico
\end{itemize}{}

\section{Definizione di Free e Closed Item Set}
Introduciamo la nozione di \textbf{free set} e \textbf{closed set} che verranno poi utilizzati dall'algoritmo \textbf{CFDMiner} che è stato implementato per trovare i CFDs in un dataset.

Un \textit{item set} è una coppia $(X, t_p)$ dove $X \subseteq attr(R)$ e $t_p$ è un pattern costante su \textit{X}. Il supporto di $(X, t_p)$ in \textit{r}, denotato con $supp(X, t_p, r)$, è definito come il set di tuple in \textit{r} che corrispondono a $t_p$ sugli \textit{X-attributes}.

Diremo che $(Y, s_p)$ è più generale di $(X, t_p)$, denotato con $(X, t_p) \preceq (Y, s_p)$, se $Y \subseteq X$ e $s_p = t_p[Y]$. Da ciò è triviale notare che $supp(X, t_p, r) \subseteq supp(Y, s_p, r)$.

\subsection{Closed item set}
Un \textit{item set} $(X, t_p)$ è detto \textit{closed} in \textit{r} se non esiste alcun \textit{item set} $(Y, s_p)$ tale che $(Y, s_p) \preceq (X, t_p)$ per cui $supp(Y, s_p, r) = supp(X, t_p, r)$. Intuitivamente non è possibile aggiungere un ulteriore attributo (vincolo) a un \textit{closed item set} $(X, t_p)$ senza decrementare il suo supporto. Denotiamo con $clo(X, t_p)$ l'unico \textit{closed item set} che estende $(X, t_p)$ e che ha uguale supporto in \textit{r}.
 
\subsection{Free item set}
Un \textbf{item set} $(X, t_p)$ è detto \textit{free} in \textit{r} se non esiste un \textit{item set} $(Y, s_p)$ tale che $(X, t_p) \preceq (Y, s_p)$ per cui $supp(Y,s_p, r) = supp(X, t_p, r)$. Intuitivamente un \textit{free item set} $(X, t_p)$ non può essere generalizzato, non si può togliere un attributo (vincolo), senza incrementare il suo supporto. 

\subsection{Connessione tra free/closed item set e left-reduced CFDs} \label{connectionFreeClosedItemSetLeftReducedCFDs}
Per un'istanza \textit{r} di \textit{R} e per ogni \textit{k-frequent left-reduced costant} CFD $\phi = (X \to A, (t_p \parallel a))$, $r \models \phi$ sse:
\begin{itemize}
    \item l'\textit{item set} $(X, t_p)$ è \textit{free, k-frequent} e non contiene $(A, a)$
    \item $clo(X, t_p) \preceq (A, a)$
    \item $(X, t_p)$ non contiene un \textit{free set} $(Y, s_p)$ più piccolo che rispetti la proprietà precedente
\end{itemize}{}

\section{FP-Tree} \label{FPTree}
Definiamo ora un \textit{Frequent Pattern Tree} (FP-Tree) \cite{FP-Tree} come struttura di supporto negli algoritmi successivi. Questa struttura conterrà i \textit{k-frequent item set} e ci permetterà di estrarre successivamente in modo efficiente i \textit{closed item set} e i \textit{free item set}. Un FP-Tree è composto da due strutture:
\begin{itemize}
    \item \textbf{Un albero dei prefissi} dove due transazioni che condividono un prefisso comune condividono una parte del cammino dal nodo radice a un nodo interno dell'albero;
    \item \textbf{Una header table} in cui ogni riga contiene due campi. L'\textbf{item name} e il \textbf{head of node-link} che punta al primo nodo nell'FP-Tree che ha quell'\textit{item name}.
\end{itemize}{}
\noindent
La radice dell'albero dei prefissi ha valore \textit{null} e come figli ha gli elementi del set dei prefissi degli item set. Ogni nodo interno dell'albero contiene invece tre valori. Il primo rappresenta il \textbf{nome dell'item} che il nodo rappresenta, il secondo indica il \textbf{numero di transazioni} rappresentate dal percorso dalla radice al nodo stesso e l'ultimo valore è detto \textbf{node-link} e porta al prossimo nodo dell'FP-Tree che ha lo stesso \textit{item name}.

\subsection{Dettagli di implementazione}
L'implementazione dell'algoritmo di costruzione dell'FP-Tree avviene nelle seguenti fasi:
\begin{itemize}
    \item Si contano i valori degli attributi delle istanze presenti nel dataset, inserendo i valori che soddisfano la soglia del minimo supporto \textit{k} nella \textit{header table} con il relativo valore del supporto calcolato
    \item Si procede rimuovendo da ogni istanza gli attributi che non sono presenti nella \textit{hash table} e riordinandoli in modo decrescente in base al supporto 
    \item Si costruisce il \textit{Prefix Tree} indicizzando le istanze modificate
    \item Per ogni valore assunto dall'attributo che viene aggiunto all'albero si inserisce il relativo puntatore al \textit{node-link} della \textit{header table}
\end{itemize}{}

\section{GC-Growth}
Utilizziamo l'algoritmo GC-Growth \cite{GCGROWTH} per trovare i \textit{k-frequent closed item set} e i \textit{k-frequent free item set} associati ad essi sfruttando la struttura definita al paragrafo \ref{FPTree}. Si è scelto questo algoritmo poiché trova contemporaneamente entrambi.

\subsection{Premesse}
L'algoritmo prende in input un FP-Tree e lo utilizza per inizializzare le seguenti liste nella prima fase dell'algoritmo:
\begin{itemize}
    \item $P[X] = |\{d_T | T \in \mathcal{D}$ e $X$ è un prefisso di $T\}|$ 
    \item $T[X]$ è vero se e solo se \textit{X} è una transazione intera
    \item $H[\alpha] = \{X | P[X]$ è definito e $\{\alpha\}$ è un suffisso di $X\}$
    \item $S[X]$ è il $sup(X, \mathcal{D})$.
\end{itemize}{}
\noindent
Definiamo poi un ordine sui nodi dell'albero secondo cui dati due nodi \textit{X} e \textit{Y} diremo che $X <_1 Y$ se e solo se \textit{X} viene prima di \textit{Y} nella visita dell'albero. La visita viene effettuata da destra a sinistra e dall'alto verso il basso.


\subsection{Pseudo-codice}
\begin{lstlisting}[mathescape]
Riempi $P[\cdot], T[\cdot], H[\cdot]$ con un accesso all'FP-Tree
Imposta $Generators[\{\}] := true$
Inizializza $Result[\cdot]$ a vuoto
Siano $X_1, X_2, ..., X_n$ i nodi nell'FP-Tree tree, dove $X_1 <_1 X_2 <_1 ... <_1 X_n$
for $i = 1, ..., n$ do
    $S[X_i] := \sum_{X \in H[last(X_i)], X_i \subseteq X} P[X]$
    if $S[X_i] \geq k$ and $\forall \alpha \in X_i : S[X_i] < S[X_i - \{\alpha\}]$ and $\forall \alpha \in X_i:Generators[X_i - \{\alpha\}]$ then
        $Generators[X_i] := true$;
        $ClosedSet := \bigcap \{X" | X' \in H[last(X_i)], X_i \subseteq X', X'$ e' un prefisso di $X", T[X"]=true \}$;
        $Result[ClosedSet] := Result[ClosedSet] \cup \{X_i\}$;
    else
        $Generators[X_i] := false$;
    end if
end for
for each $ClosedSet$ tale che $Result[ClosetSet] \neq \{\}$ do
    output $Result[ClosedSet], ClosedSet$
end for

\end{lstlisting}

\subsection{Commento}
L'algoritmo GC-Growth identifica i closed item set e i free item set in base alla variazione del supporto durante l'attraversamento dell'albero. L'attraversamento di un cammino avviene tramite pattern matching: un nodo $X_i$ rappresenta uno specifico item set $X_i$ e i nodi che compongono il cammino radice-nodo rappresentano gli attributi che compongono l'item set.

La visita dell'albero avviene utilizzando l'ordine $<_1$ definito precedentemente. Il supporto totale di un item $X_i$ viene calcolato attraverso la somma dei supporti dei prefissi che contengono l'item set $X_i$, indicizzati sull'FP-Tree.

\subsubsection*{Individuazione dei free item set}
Se l'item set $X_i$ è \textit{k-frequent}, ogni possibile sottoinsieme dell'item set $X_i$ ha un supporto maggiore di $X_i$ e ogni sottoinsieme dell'item set $X_i$ è un free item set, allora anche $X_i$ è un free item set. L'aggiunta di un nodo al cammino rappresenta l'aggiunta di un attributo all'item set: se il supporto diminuisce allora l'item set $X_i$ è un free item set, indicato nell'algoritmo come \textit{generators}.

\subsubsection*{Individuazione dei closed item set}
La ricerca di un closed item set procede visitando il sotto-albero del nodo $X_i$, generando tutti i pattern da $X_i$ alle foglie del sotto-albero. Il procedimento viene ripetuto per tutti i nodi che appartengono alla \textit{node-links} di $X_i$ contenuta nella \textit{header table} dell'FP-Tree. Infine, si mantengono gli attributi comuni ai cammini visitati, generando così un closed item set, e si accoda $X_i$ alla lista dei free item set del closed item set.

\section{CFDMiner}
L'algoritmo implementato per trovare i CFDs in un database è \textbf{CFDMiner}~\cite{CFDMiner}. In particolare trova i \textbf{CFDs k-frequent, minimali e costanti} data un'istanza \textit{r} di \textit{R} e una soglia \textit{k}. L'algoritmo si basa sulla relazione tra CFD \textit{left-reduced} costanti e \textit{free/closed item set} e sulle relative proprietà discusse precedentemente nel paragrafo \ref{connectionFreeClosedItemSetLeftReducedCFDs}.

\subsection{Premesse}
L'algoritmo assume di avere a disposizione un'istanza \textit{r} di \textit{R}, una soglia \textit{k}, i \textit{k-frequent closed item sets} e i corrispondenti \textit{k-frequent free item sets}. Con questi set a disposizione \textbf{CFDMiner} restituirà una lista con tutti i \textit{CFD k-frequent, left-reduced} costanti.

Per ottenere i \textit{k-frequent closed} e \textit{free item sets} è stato utilizzato l'algoritmo \textbf{GC-Growth} che trova simultaneamente i \textit{k-frequent closed item set} e i relativi \textit{k-frequent free item set}.

\subsection{Pseudo-codice}
\begin{lstlisting}[mathescape]
Calcola una mappa C2F che associa ad ogni k-frequent closed item set in r i corrispondenti k-frequent free item set 
for all k-frequent closed item set $(Y, s_p)$ in r do
    sia L la lista di tutti i free item set in C2F$(Y, s_p)$;
    for all $(X, t_p) \in L$ do
        inizializza $RHS(X, t_p) := (Y-X, s_p[Y-X])$;
    end for
    for all $(X, t_p) \in L$ do
        for all $(X', t_p[X']) \in L$ tale che $X' \subset X$ do
            $RHS(X, t_p) := RHS(X, t_p) \cap RHS(X', t_p[X'])$;
        end for
        if $RHS(X, t_p) \neq \emptyset$ then 
            return $(X \to A, (t_p \parallel a))$ for all $(A, a) \in RHS(X, t_p)$;
        end if
    end for
end for
\end{lstlisting}

\subsection{Commento}
Per ogni \textit{closed item set} l'algoritmo itera sulla lista di \textit{free item set} associati. Per generare un potenziale RHS, relativo a un \textit{free item set}, si calcola la differenza tra il closed item set correlato e il \textit{free item set}. Ciò consente di individuare solo i CFDs non triviali, cioè quelli in cui $A \notin X$.

Per ogni \textit{free item set} $X$ l'algoritmo cerca tutti i \textit{free item set} $X'$ nella stessa lista che sono sottoinsiemi di $X$. Il candidato RHS di $X$ viene aggiornato facendone l'intersezione con l'RHS d $X'$.

Dopo aver iterato sulla lista dei \textit{free item set}, l'RHS trovato è sicuramente \textit{left-reduced}, ossia dato un sotto-insieme $Y$ dell'LHS $X$ del CFD con l'RHS trovato, non esiste un CFD con LHS $Y$ e RHS trovato che soddisfi $r$. L'RHS calcolato, essendo non triviale e left-reduced, risulta quindi essere minimale.

Se l'RHS di $X$ non è un insieme vuoto allora, per ogni suo RHS viene generato un CFD avente per LHS il free item set $X$ e per RHS un attributo del candidato RHS di $X$.

\newpage
\section{Risultati sperimentali}
In questo capitolo verranno presentati i risultati ottenuti dall'esecuzione dell'algoritmo implementato su tre diversi dataset con numero di attributi e cardinalità differenti. I tre dataset utilizzati sono:
\begin{itemize}
    \item Wisconsin Breast Cancer (WBC) \cite{WBC} - 699 istanze e 11 attributi  
    \item Census \cite{Census} - 48842 istanze e 15 attributi
    \item Mushroom \cite{Mushroom} - 8124 istanze e 23 attributi
\end{itemize}{}

\subsection{Descrizione dei dataset}
Il Wisconsin Breast Cancer (WBC) dataset raccoglie i parametri associati alle diagnosi di esami sul tumore al seno. Il dataset è composto da 699 istanze e 11 attributi di tipo intero. Le istanze sono suddivise in due classi: tumore benigno e tumore maligno. 

Il dataset Census Income raccoglie informazioni relative al reddito annuale di una serie di soggetti di cui conosciamo quattordici caratteristiche, tra cui: etnia, sesso, lavoro e stato di nascita. Le istanze sono suddivise in coloro che percepiscono più e meno di 50k dollari annui. Elemento rilevante del dataset è la cardinalità delle istanze molto superiore a quella degli altri dataset.

Il terzo e ultimo dataset, Mushroom, raccoglie informazioni riguardo alle caratteristiche di esemplari di funghi suddividendoli tra velenosi e non. Notare che il numero di attributi è superiore a quello degli altri dataset e che sono tutti di tipo categorico. 

\subsection{Analisi dei CFDs trovati in funzione del supporto}
Il primo aspetto sul quale è necessario porre attenzione è la variazione del numero dei CFDs trovati al variare del supporto, cioè del valore di soglia \textit{k}. Per ogni dataset vengono riportati i risultati ottenuti nelle tabelle \ref{WisconsinBreastCancerCFDs}-\ref{CensusCFDs}-\ref{MushroomCFDs}.

Dove è stato inserito il simbolo "-" non sono stati ottenuti risultati in quanto la computazione richiede troppo tempo e/o troppe risorse computazionali: questo è dovuto al fatto che trovare i CFDs di un dataset è un problema NP-Completo e richiede che venga affrontato introducendo varie ottimizzazioni ed euristiche per ottenere soluzioni in presenza di dataset numerosi e/o con molti attributi.

Nonostante l'aggiunta di ottimizzazioni, quali l'\textit{header table} di FP-Tree e tabelle di hashing per manipolare risultati, il tempo richiesto per alcune configurazioni risulta essere esponenziale.


\begin{table}[ht]
    \center
    \begin{tabular}{|c|c|}
        \hline
        Supporto \textit{k} & CFDs trovati \\
        \hline
        0.1 & 545 \\
        0.2 & 84 \\
        0.4 & 65 \\
        0.8 & 0 \\
        \hline
    \end{tabular}
    \caption{Wisconsin Breast Cancer}
    \label{WisconsinBreastCancerCFDs}
\end{table}
    
\begin{table}[ht]
    \center
    \begin{tabular}{|c|c|}
        \hline
        Supporto \textit{k} & CFDs trovati \\
        \hline
        0.1 & - \\
        0.2 & 98 \\
        0.4 & 28 \\
        0.8 & 6 \\
        \hline
    \end{tabular}
    \caption{Census}
    \label{CensusCFDs}
\end{table}
    
\begin{table}[ht]
    \center
    \begin{tabular}{|c|c|}
        \hline
        Supporto \textit{k} & CFDs trovati \\
        \hline
        0.1 & - \\
        0.2 & - \\
        0.4 & 282 \\
        0.8 & 14 \\
        \hline
    \end{tabular}
    \caption{Mushroom}
    \label{MushroomCFDs}
\end{table}
\noindent
Il numero di CFDs riportato nelle tabelle aumenta in modo direttamente proporzionale all'arietà del dataset. Nonostante per l'ultimo dataset siano riportati solo due dataset si può osservare come a parità del valore di \textit{k} il numero di CFDs trovati è significativamente superiore rispetto agli altri.

\subsection{Analisi del tempo di esecuzione in funzione del supporto}
Alla luce del fatto che la natura del problema è esponenziale, un ulteriore parametro da tenere in considerazione è il tempo di esecuzione dell'algoritmo. Come riportato nelle tabelle \ref{WisconsinBreastCancerTime}-\ref{CensusTime}-\ref{MushroomTime} non tutte le computazioni sono terminate nel tempo limite prestabilito di trenta minuti.

\begin{table}[H]
    \center
    \begin{tabular}{|c|c|}
        \hline
        Supporto \textit{k} & Tempo (\textit{s}) \\
        \hline
        0.1 & 19.38 \\
        0.2 & 1.15 \\
        0.4 & 0.58 \\
        0.8 & 0.53 \\
        \hline
    \end{tabular}
    \caption{Wisconsin Breast Cancer}
    \label{WisconsinBreastCancerTime}
\end{table}
    
\begin{table}[H]
    \center
    \begin{tabular}{|c|c|}
        \hline
        Supporto \textit{k} & Tempo (\textit{s}) \\
        \hline
        0.1 & - \\
        0.2 & 54.73 \\
        0.4 & 1.01 \\
        0.8 & 0.89 \\
        \hline
    \end{tabular}
    \caption{Census}
    \label{CensusTime}
\end{table}
    
\begin{table}[H]
    \center
    \begin{tabular}{|c|c|}
        \hline
        Supporto \textit{k} & Tempo (\textit{s}) \\
        \hline
        0.1 & - \\
        0.2 & - \\
        0.4 & 99.83 \\
        0.8 & 0.73 \\
        \hline
    \end{tabular}
    \caption{Mushroom}
    \label{MushroomTime}
\end{table}
\noindent
Il tempo di esecuzione dell'algoritmo su ogni dataset conferma la necessità di ottimizzare le operazioni di calcolo. All'aumentare sia della cardinalità delle istanze che dell'arietà del dataset il tempo aumenta, ma in particolare si osserva che il fattore che contribuisce maggiormente è il numero di attributi. 

Infatti nella tabella \ref{MushroomTime}, relativa al dataset \textit{Mushroom} con 23 attributi e 8124 istanze, si può notare come il tempo impiegato dall'algoritmo è di un'ordine di grandezza superiore rispetto al dataset \textit{Census}, composto da 15 attributi e 48842 istanze.

\section{Conclusioni}
L'obiettivo prefissato per il progetto è quello di trovare i CFDs, utili per verificare la consistenza di un dataset. La scelta dell'algoritmo è ricaduta su \textit{CFDMiner} in quanto risulta l'algoritmo più rapido per la scoperta dei CFDs costanti.

Per poter realizzare una versione funzionante ed efficiente di questo algoritmo è stato necessario implementare ulteriori algoritmi che forniscano l'input richiesto da \textit{CFDMiner}: i \textit{closed item set} e i relativi \textit{free item set}. Questi ultimi sono stati individuati in contemporanea tramite \textit{GC-Growth}. L'algoritmo \textit{FP-Tree} assolve al compito di ridurre drasticamente il numero di interrogazioni necessarie al database per il calcolo dei supporti dei vari \textit{item set}, effettuando un'indicizzazione dei prefissi delle tuple.

La nostra implementazione degli algoritmi ha prodotto in output dei CFDs coerenti in tempo utile, nonostante il problema sia NP-Completo e molto sensibile al numero di attributi del dataset.

\begin{thebibliography}{9}

\bibitem{FP-Tree}
Jiawei Han, Jian Pei, Yiwen Yin, "Mining Frequent Patterns without Candidate Generation", \textit{In Proceedings of 19th ACM-SIGMOD International Conference on Management of Data}, pages 1–12, 2000.

\bibitem{GCGROWTH} 
H. Li, J. Li, L. Wong, M. Feng, and Y.-P. Tan, “Relative Risk and Odds Ratio: A Data Mining Perspective,” \textit{Proc. 24th ACM SIGMOD-SIGACT-SIGART Symp. Principles of Database Systems (PODS ’05)}, pp. 368-377, 2005.

\bibitem{CFDMiner} 
Wenfei Fan, Floris Geerts, Jianzhong Li, Ming Xiong, "Discovering Conditional Functional Dependencies", \textit{IEEE TRANSACTIONS ON KNOWLEDGE AND DATA ENGINEERING}, VOL. 23, NO. 5, MAY 2011, pp. 683-698, 2011. 

\bibitem{WBC}
Wisconsin Breast Cancer - https://archive.ics.uci.edu/ml/datasets/breast+cancer+wisconsin+(original)

\bibitem{Census}
Census Income - https://archive.ics.uci.edu/ml/datasets/Adult

\bibitem{Mushroom}
Mushroom - https://archive.ics.uci.edu/ml/datasets/mushroom

 
\end{thebibliography}


\end{document}


